//
// Created by yuiko on 12/5/22.
//
#define _DEFAULT_SOURCE
#define HEAP_INIT_SIZE 10000

#include "tests.h"
#include <stddef.h>
#include <stdio.h>
#include <sys/mman.h>

static struct block_header* find_block(void* data) {
    return (struct block_header *) ((uint8_t *) data - offsetof(struct block_header, contents));
}



bool test_alloc() {
    void *heap = heap_init(HEAP_INIT_SIZE);
    void *test = _malloc(1000);
    debug_heap(stdout, heap);
    if (test == NULL) {
        err("FAILED\n");
        return false;
    }
    _free(test);
    munmap(heap, size_from_capacity((block_capacity) {.bytes=HEAP_INIT_SIZE}).bytes);
    return true;
}

bool test_free_one_block() {
    void *heap = heap_init(HEAP_INIT_SIZE);
    void *arr1 = _malloc(100);
    void *arr2 = _malloc(100);
    void *arr3 = _malloc(100);
    void *arr4 = _malloc(100);
    struct block_header *block3 = find_block(arr3);
    _free(arr3);
    if(!block3->is_free){
        err("Warning: Problems with _free\n");
    }else{
        printf("Success\n");
    }
    
    _free(arr1);
    _free(arr2);
    _free(arr4);
    munmap(heap, size_from_capacity((block_capacity) {.bytes=HEAP_INIT_SIZE}).bytes);
    return true;
}

bool test_free_two_block() {
    void *heap = heap_init(HEAP_INIT_SIZE);
    void *arr1 = _malloc(100);
    void *arr2 = _malloc(100);
    void *arr3 = _malloc(100);
    void *arr4 = _malloc(100);
    void *arr5 = _malloc(100);
    _free(arr3);
    _free(arr5);
    struct block_header *block3 = find_block(arr3);
    struct block_header *block5 = find_block(arr5);
    if(!block3->is_free || !block5->is_free){
        err("Warning: Problems with _free\n");
    }else{
        printf("Success\n");
    }
    _free(arr1);
    _free(arr2);
    _free(arr4);
    munmap(heap, size_from_capacity((block_capacity) {.bytes=HEAP_INIT_SIZE}).bytes);
    return true;
}

bool test_memoryIsFull_NewBlockIsExpandsOld() {
    void *heap = heap_init(HEAP_INIT_SIZE);
    debug_heap(stdout, heap);
    void *block = _malloc(HEAP_INIT_SIZE + 8000);
    debug_heap(stdout, heap);
    _free(block);
    munmap(heap, size_from_capacity((block_capacity) {.bytes=HEAP_INIT_SIZE + 100}).bytes);
    return true;
}

bool test_CanNotExpandBlock() {
    void *heap = heap_init(HEAP_INIT_SIZE);
    struct block_header *heapHeader = (struct block_header *) heap;
    void *block = _malloc(heapHeader->capacity.bytes);
    debug_heap(stdout, heap);
    void *blockAfterHeap = mmap(heapHeader->contents + heapHeader->capacity.bytes, HEAP_INIT_SIZE*2, PROT_READ | PROT_WRITE,
                                MAP_PRIVATE, -1, 0);
    void *blockAfterMmapedBlock = _malloc(100);
    debug_heap(stdout, heap);
    _free(block);
    _free(blockAfterMmapedBlock);
    munmap(blockAfterHeap, size_from_capacity((block_capacity) {.bytes=4000}).bytes);
    munmap(heap, size_from_capacity((block_capacity) {.bytes=HEAP_INIT_SIZE}).bytes);
    return true;
}

void run() {
    printf("Test_alloc is running\n");
    if (test_alloc()) {
        printf("Success\n");
    }

    printf("\n\n");
    printf("Test_free_one_block is running\n");
    test_free_one_block();

    printf("\n\n");
    printf("Test_free_two_block is running\n");
    test_free_two_block();

    printf("\n\n");
    printf("Test_memoryIsFull_NewBlockIsExpandsOld is running\n");
    test_memoryIsFull_NewBlockIsExpandsOld();
    printf("Success\n");

    printf("\n\n");
    printf("Test_CanNotExpandBlock is running\n");
    test_CanNotExpandBlock();
    printf("Success\n");

}
